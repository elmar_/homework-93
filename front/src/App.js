import React from 'react';
import {Switch, Route, Redirect, Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Calendar from "./containers/Calendar/Calendar";
import {Button, Layout} from "antd";
import usersSlice from "./store/slices/usersSlice";
import CreateEvent from "./containers/CreateEvent/CreateEvent";
import EditEvent from "./containers/EditEvent/EditEvent";
import AddFriend from "./containers/AddFriend/AddFriend";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo}/>;
};

const {Header, Content} = Layout;
const {logoutRequest} = usersSlice.actions;

const App = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const logout = () => {
    dispatch(logoutRequest());
  };

  return (
  <Layout>
    <Header style={{color: '#fff', display: 'flex', justifyContent: 'space-between', padding: '0 100px'}}>
      <div>
        <Link to='/' style={{textTransform: "uppercase", color: "#fff"}}>
          Date
        </Link>
      </div>
      {/*<Menu theme="dark" mode="horizontal" selectable={false}>*/}
      {/*  <Menu.Item key="1">nav 1</Menu.Item>*/}
      {/*  <Menu.Item key="2">nav 2</Menu.Item>*/}
      {/*  <Menu.Item key="3">nav 3</Menu.Item>*/}
      {/*</Menu>*/}
      {user && <div>Hello {user.displayName} <Button type='primary' onClick={logout}>Logout</Button></div>}
    </Header>
    <Content style={{ padding: '0 50px', backgroundColor: '#fff' }}>
      <Switch>
        <ProtectedRoute
          path="/"
          exact
          component={Calendar}
          isAllowed={user}
          redirectTo="/login"
        />
        <ProtectedRoute
          path="/edit/:id"
          exact
          component={EditEvent}
          isAllowed={user}
          redirectTo="/login"
        />
        <ProtectedRoute
          path="/addFriend"
          exact
          component={AddFriend}
          isAllowed={user}
          redirectTo="/login"
        />
        <ProtectedRoute
          path="/createEvent"
          exact
          component={CreateEvent}
          isAllowed={user}
          redirectTo="/login"
        />
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
      </Switch>
    </Content>
    </Layout>
  );
};

export default App;
