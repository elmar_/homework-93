import {all} from "redux-saga/effects";
import historySagas from "./sagas/historySagas";
import history from "../history";
import usersSagas from "./sagas/usersSagas";
import eventsSagas from "./sagas/eventsSagas";


function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...usersSagas,
    ...eventsSagas,

  ]);
}

export default rootSaga;
