import eventsSlice from "../slices/eventsSlice";
import axiosApi from "../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {historyPush} from "../actions/historyActions";
import {notificationError, notificationSuccess} from "../../components/Notifications/Notifications";

const {
  createEventError,
  createEventSuccess,
  createEventRequest,
  fetchEventsError,
  fetchEventsRequest,
  fetchEventsSuccess,
  deleteEventError,
  deleteEventRequest,
  deleteEventSuccess,
  getOneError,
  getOneRequest,
  getOneSuccess,
  editEventError,
  editEventRequest,
  editEventSuccess,

} = eventsSlice.actions;

function* createEvent({payload: event}) {
  try {
    const response = yield axiosApi.post('/events', event);
    yield put(createEventSuccess(response.data));
    yield put(historyPush('/'));
    notificationSuccess('Event created');
  } catch (error) {
    notificationError('Event was not created');
    yield put(createEventError(error.response.data || error));
  }
}

function* fetchEvent() {
  try {
    const response = yield axiosApi.get('/events');
    yield put(fetchEventsSuccess(response.data));
  } catch (error) {
    yield put(fetchEventsError(error.response.data || error));
  }
}

function* deleteEvent({payload}) {
  try{
    const id = payload.id;
    yield axiosApi.delete('/events', {data: {id}});
    yield put(deleteEventSuccess(payload));
  } catch (e) {
    console.log(e);
    notificationError('Event was not deleted');
    yield put(deleteEventError(e?.response.data || e));
  }
}

function* getOne({payload: id}) {
  try{
    const response = yield axiosApi.get('/events/' + id);
    yield put(getOneSuccess(response.data));
  } catch (e) {
    console.log(e);
    notificationError('Event was not fetched');
    yield put(getOneError(e?.response?.data || e));
  }
}

function* editEvent({payload: event}) {
  try{
    console.log(event);
    const response = yield axiosApi.put('/events', event);
    yield put(editEventSuccess(response.data));
  } catch (e) {
    console.log(e);
    notificationError('Event was not edited');
    yield put(editEventError(e?.response.data || e));
  }
}


const eventsSagas = [
  takeEvery(createEventRequest, createEvent),
  takeEvery(fetchEventsRequest, fetchEvent),
  takeEvery(deleteEventRequest, deleteEvent),
  takeEvery(getOneRequest, getOne),
  takeEvery(editEventRequest, editEvent),

];

export default eventsSagas;

