import {put, takeEvery} from "redux-saga/effects";
import usersSlice from "../slices/usersSlice";
import {historyPush} from "../actions/historyActions";
import axiosApi from "../axiosApi";
import {notificationError, notificationSuccess} from "../../components/Notifications/Notifications";

const {
  facebookLoginRequest,
  loginFailure,
  loginRequest,
  loginSuccess, logoutRequest, logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess,
  friendRequest,
  friedFailure,
  friendSuccess
} = usersSlice.actions;


export function* registerUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users', userData);
    yield put(registerSuccess(response.data));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    notificationSuccess('Login successful');
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* facebookLogin({payload: data}) {
  try {
    const response = yield axiosApi.post('/users/facebookLogin', data);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    notificationSuccess('Login success');
  } catch (error) {
    yield put(loginFailure(error.response.data));
    notificationError('Login failed');
  }
}

export function* logout() {
  try {
    yield axiosApi.delete('/users/sessions');
    yield put(logoutSuccess());
    yield put(historyPush('/'));
  } catch (e) {
    notificationError('Logout failed');
  }
}

export function* addFriend({payload: friendId}) {
  try {
    const response = yield axiosApi.put('/users/addFriend', friendId);
    yield put(friendSuccess(response.data));
    yield put(historyPush('/'));
    notificationSuccess('Friend added');
  } catch (error) {
    yield put(friedFailure(error.response.data));
  }
}


const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(facebookLoginRequest, facebookLogin),
  takeEvery(logoutRequest, logout),
  takeEvery(friendRequest, addFriend)
];

export default usersSagas;
