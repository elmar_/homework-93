import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  events: [],
  createEventLoading: false,
  createError: null,
  fetchEventsLoading: false,
  fetchError: null,
  deleteLoading: false,
  deleteError: null,
  oneLoading: false,
  oneError: false,
  event: {},
  editLoading: false,
  editError: false,
};

const name = 'event';

const eventsSlice = createSlice({
  name,
  initialState,
  reducers: {
    createEventRequest: state => {
      state.fetchEventsLoading = true;
    },
    createEventSuccess: (state, {payload: event}) => {
      state.fetchEventsLoading = false;
      state.createError = null;
      state.events = [...state.events, event];
    },
    createEventError: (state, {payload: error}) => {
      state.createError = error;
      state.fetchEventsLoading = false;
    },
    fetchEventsRequest: state => {
      state.fetchEventsLoading = true;
    },
    fetchEventsSuccess: (state, {payload: events}) => {
      state.events = events;
      state.fetchEventsLoading = false;
      state.fetchError = null;
    },
    fetchEventsError: (state, {payload: error}) => {
      state.fetchEventsLoading = false;
      state.fetchError = error;
    },
    deleteEventRequest: state => {
      state.deleteLoading = true;
    },
    deleteEventSuccess: (state, {payload: {index}}) => {
      state.events.splice(index, 1);
      state.deleteLoading = false;
      state.deleteError = null;
    },
    deleteEventError: (state, {payload: error}) => {
      state.deleteLoading = false;
      state.deleteError = error;
    },
    getOneRequest: state => {
      state.oneLoading = true;
    },
    getOneSuccess: (state, {payload: event}) => {
      state.event = event;
      state.oneLoading = false;
      state.oneError = null;
    },
    getOneError: (state, {payload: error}) => {
      state.oneLoading = false;
      state.oneError = error;
    },
    editEventRequest: state => {
      state.editLoading = true;
    },
    editEventSuccess: (state, {payload: event}) => {
      console.log(event)
      const index = state.events.findIndex(item => toString(item._id) === toString(event._id));
      console.log(index);
      state.events[index] = event;
      state.editLoading = false;
      state.editError = null;
      state.event = null;
    } ,
    editEventError: (state, {payload: error}) => {
      state.editError = error;
      state.editLoading = false;
    }



  }
});


export default eventsSlice;
