import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null,
  user: null,
  friendLoading: false,
  friendError: null
};

const name = 'users';

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerRequest: state => {
      state.registerLoading = true;
    },
    registerSuccess: (state, {payload: user}) => {
      state.registerLoading = false;
      state.user = user;
      state.registerError = null;
      state.loginError = null;
    },
    registerFailure: (state, {payload: error}) => {
      state.registerLoading = false;
      state.registerError = error;
    },
    loginRequest: state => {
      state.loginLoading = true;
    },
    loginSuccess: (state, {payload: user}) => {
      state.loginLoading = false;
      state.user = user;
      state.registerError = null;
      state.loginError = null;
    },
    loginFailure: (state, {payload: error}) => {
      state.loginLoading = false;
      state.loginError = error;
    },
    logoutRequest: () => {},
    logoutSuccess: state => {
      state.user = null;
    },
    facebookLoginRequest: state => {
      state.loginLoading = true;
    },
    friendRequest: state => {
      state.friendLoading = true;
    },
    friendSuccess: (state, {payload: user}) => {
      console.log(user)
      state.user = user;
      state.friendLoading = false;
      state.friendError = null;
    },
    friedFailure: (state, {payload: error}) => {
      state.friendError = error;
      state.friendLoading = false;
    }

  }
});

export default usersSlice;
