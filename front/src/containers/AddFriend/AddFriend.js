import React from 'react';
import {Alert, Button, DatePicker, Form, Input} from "antd";
import moment from "moment";
import {useDispatch, useSelector} from "react-redux";
import usersSlice from "../../store/slices/usersSlice";

const layout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 10,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 7,
    span: 10,
  },
};

const {friendRequest} = usersSlice.actions;


const AddFriend = () => {
  const loading = useSelector(state => state.users.friendLoading);
  const error = useSelector(state => state.users.friendError);
  const dispatch = useDispatch();

  const onFinish = data => {
    dispatch(friendRequest(data));
  };

  return (
    <div style={{paddingTop: 200}}>
      <Form
        {...layout}
        onFinish={onFinish}
      >
        {<Form.Item {...tailLayout}>
          {error && <Alert
            message="Error Text"
            description={error.message || error.global || "Error, try again"}
            type="error"
          />}
        </Form.Item>}
        <Form.Item
          label="Input email"
          name="email"
          type="email"
          rules={[
            {
              required: true,
              message: 'Please input your friends email!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" loading={loading}>
            Add friend
          </Button>
        </Form.Item>

      </Form>
    </div>
  );
};

export default AddFriend;
