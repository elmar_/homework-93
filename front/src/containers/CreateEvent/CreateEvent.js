import React from 'react';
import {Alert, Button, DatePicker, Form, Input} from "antd";
import {useDispatch, useSelector} from "react-redux";
import moment from "moment";
import eventsSlice from "../../store/slices/eventsSlice";

const layout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 10,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 7,
    span: 10,
  },
};

const {createEventRequest} = eventsSlice.actions;

const CreateEvent = () => {
  const loading = useSelector(state => state.events.createEventLoading);
  const error = useSelector(state => state.events.createError);
  const dispatch = useDispatch();

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  const onFinish = data => {
    dispatch(createEventRequest(data));
  };

  return (
    <div style={{paddingTop: 200}}>
      <Form
        {...layout}
        onFinish={onFinish}
      >
        {<Form.Item {...tailLayout}>
          {error && <Alert
            message="Error Text"
            description={error.message || error.global || "Error, try again"}
            type="error"
          />}
        </Form.Item>}
        <Form.Item
          label="Event name"
          name="title"
          rules={[
            {
              required: true,
              message: 'Please input your event!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Duration"
          name="duration"
          rules={[
            {
              required: true,
              message: 'Please input duration of event!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Date"
          name="date"
          rules={[
            {
              required: true,
              message: 'Please input date!',
            },
          ]}
        >
          <DatePicker
            format="YYYY-MM-DD HH:mm"
            disabledDate={disabledDate}
            showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
          />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" loading={loading}>
            Add event
          </Button>
        </Form.Item>

      </Form>
    </div>
  );
};

export default CreateEvent;
