import React, {useEffect, useState} from 'react';
import {Button, List, Typography} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import eventsSlice from "../../store/slices/eventsSlice";
import {formatDate} from "../../components/FormateDate/FormatDate";

const {fetchEventsRequest, deleteEventRequest} = eventsSlice.actions;

const Calendar = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const events = useSelector(state => state.events.events);
  const shared = user.shared;
  const friends = user.friends;


  useEffect(() => {
    dispatch(fetchEventsRequest() );
  }, [dispatch]);

  const deleteEvent = (id, index) => {
    dispatch(deleteEventRequest({id, index}));
  };


  return (
    <>
      <div style={{padding: '20px 0'}}>
        <Link to='/createEvent' component={Button} type="primary" style={{marginRight: 20}}>Create event</Link>
        <Link to='/addFriend' component={Button} type="primary">Add friend</Link>
      </div>

      <div style={{display: 'flex', marginTop: 20, justifyContent: "space-between"}}>
        <div style={{width: '60%'}}>
          <List
            header={<div>Events</div>}
            bordered
            dataSource={events}
            renderItem={(item, i) => (
              <List.Item style={{display: 'flex', justifyContent: 'space-between', cursor: 'pointer'}}>
                <div>
                  <Typography.Text mark style={{marginRight: 20}}>[{formatDate(item.date)}]</Typography.Text>
                  {item.title}
                </div>
                {user._id === item.user ?
                  <div>
                    <span style={{marginRight: 15}}>Duration: {item.duration}</span>
                    <Button type="ghost" danger onClick={() => deleteEvent(item._id, i)}>Delete</Button>
                    <Link type="ghost" component={Button} to={'/edit/' + item._id}>Edit</Link>
                  </div>
                : null}
              </List.Item>
            )}
          />
        </div>

        <div style={{width: '35%'}}>
          <List
            header={<div>Friends (people whom you shared your events)</div>}
            bordered
            style={{marginBottom: 20}}
            dataSource={shared}
            renderItem={item => (
              <List.Item>
                {item.displayName}
              </List.Item>
            )}
          />

          <List
            header={<div>Friends (you can see their events)</div>}
            bordered
            dataSource={friends}
            renderItem={item => (
              <List.Item>
                {item.displayName}
              </List.Item>
            )}
          />
        </div>

      </div>
    </>
  );
};

export default Calendar;
