import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import eventsSlice from "../../store/slices/eventsSlice";
import {Alert, Button, DatePicker, Form, Input} from "antd";
import moment from "moment";

const {getOneRequest, editEventRequest} = eventsSlice.actions;

const layout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 10,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 7,
    span: 10,
  },
};

const EditEvent = (props) => {
  const id = props.match.params.id;
  const dispatch = useDispatch();
  const loading = useSelector(state => state.events.editLoading);
  const error = useSelector(state => state.events.editError);
  const event = useSelector(state => state.events.event);
  const [state, setState] = useState(event || {
    title: '',
    date: '',
    duration: ''
  });

  useEffect(() => {
    dispatch(getOneRequest(id));
  }, [dispatch, id]);

  useEffect(() => {
    setState(event || {
      title: '',
      date: '',
      duration: ''
    });
  }, [event])

  const onFinish = () => {
    console.log(state)
    dispatch(editEventRequest(state));
  };

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
  }

  const changeHandler = e => {
    const {value, name} = e.target;


    setState(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  return (
    <div>
      <Form
        {...layout}
        onFinish={onFinish}
      >
        {<Form.Item {...tailLayout}>
          {error && <Alert
            message="Error Text"
            description={error.message || error.global || "Error, try again"}
            type="error"
          />}
        </Form.Item>}

        <div style={{margin: "0 200px 10px"}}>
          <Input name="title" value={state?.title} onChange={e => changeHandler(e)} />
        </div>

        <div style={{margin: "0 200px 10px"}}>
          <Input name="duration" value={state?.duration} onChange={e => changeHandler(e)} />
        </div>

        <div>
          <DatePicker
            style={{margin: "0 200px 10px"}}
            format="YYYY-MM-DD HH:mm"
            disabledDate={disabledDate}
            showTime={{ defaultValue: moment('00:00', 'HH:mm') }}
            name="date"
          />
        </div>

        <div style={{margin: "0 200px 10px"}}>
          <Button type="primary" htmlType="submit" loading={loading}>
            Add event
          </Button>
        </div>

      </Form>
    </div>
  );
};

export default EditEvent;
