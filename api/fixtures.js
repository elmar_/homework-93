const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Event = require('./models/Event');
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@test',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'user',
  }, {
    email: 'admin@test',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'admin'
  });

  await user.updateOne({friends: [admin], shared: [admin]});
  await admin.updateOne({friends: [user], shared: [user]});

  await Event.create({
    title: 'eventByUser',
    date: new Date(1),
    duration: '12 min',
    user: user
  }, {
    title: 'eventByUser2',
    date: new Date(),
    duration: '10 min',
    user: user
  },{
    title: 'eventByAdmin',
    date: new Date(),
    duration: '12 min',
    user: admin
  }, {
    title: 'eventByAdmin2',
    date: new Date(5),
    duration: '10 min',
    user: admin
  });



  await mongoose.connection.close();
};

run().catch(console.error);
