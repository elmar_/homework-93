const express = require('express');
const mongoose = require('mongoose');
const Event = require('../models/Event');
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {
  try {
    const event = new Event({
      title: req.body.title,
      duration: req.body.duration,
      date: req.body.date,
      user: req.user._id
    });

    await event.save();

    return res.send(event);
  } catch (e) {
    return res.status(400).send({message: 'error in post event'});
  }
});

router.get('/', auth, async (req, res) => {
  try {
    const user = req.user;
    const allEvents = [];
    for (const friend of user.friends) {
      const events = await Event.find({user: friend._id});
      allEvents.push(...events);
    }

    const events = await Event.find({user: user._id});
    allEvents.push(...events);

    allEvents.sort((a, b) => {
      return new Date(a.date) - new Date(b.date);
    });

    return res.send(allEvents);
  } catch (e) {
    console.log(e)
    return res.status(400).send({message: 'error in get event'});
  }
});

router.get('/:id', auth, async (req, res) => {
  try {
    const id = req.params.id;

    const event = await Event.findOne({_id: id});

    if (toString(event.user) !== toString(req.user._id)) {
      return res.status(401).send({message: 'you going to get not your event'})
    }

    return res.send(event);
  } catch (e) {
    return res.status(400).send({message: 'error in get/:id event'});
  }
});

router.put('/', auth, async (req, res) => {
  try {
    const newEvent = req.body;
    const event = await Event.findOne({_id: newEvent._id});

    if (toString(event.user) !== toString(req.user._id)) {
      return res.status(401).send({message: 'you going to edit not your event'})
    }
    await event.delete();

    let ev = new Event({
      title: newEvent.title,
      date: newEvent.date,
      duration: newEvent.duration,
      user: newEvent.user
    });
    await ev.save();

    return res.send(ev);
  } catch (e) {
    console.log(e)
    return res.status(400).send({message: 'error in put event'});
  }
});

router.delete('/', auth, async (req, res) => {
  try {
    const event = await Event.findOne({_id: req.body.id});


    if (event.user.toString() !== req.user._id.toString()) {
      return res.status(401).send({message: 'you going to delete not your event'})
    }

    await event.delete();

    return res.send({message: 'deleted successful'});
  } catch (e) {
    return res.status(400).send({message: 'error in delete event'});
  }
});


module.exports = router;

